import java.io.Console
import sun.font.TrueTypeFont

/**
 * Created with IntelliJ IDEA.
 * User: oriol
 * Date: 14/10/13
 * Time: 16:32
 * To change this template use File | Settings | File Templates.
 */

object Tennis extends App{
  println("Select one of the following options:\n")
  println("0: Show scoreboard")
  println("1: Player1 scores a point")
  println("2: Player2 scores a point")


  //Game control vars
  var endGame = false;
  var repeat = true;

  //Player1 vars
  var score1 = 0
  var adv1 = false

  //Player2 vars
  var score2 = 0
  var adv2 = false

  while(repeat)
  {
    score1 = 0
    score2 = 0
    adv1 = false
    adv2 = false
    endGame = false

    while(!endGame)
    {
        println("\nInsert number option:")
        var ln = readLine()
        (ln) match {
          case "0" => {
            if(score1 == 40 && score2 == 40){
              if(adv1) println("Advantage Player1")
              else if(adv2) println("Advantage Player2")
              else println("Deuce")
            }
            println("Player1: " + score1 + " - " + score2 + " :Player2")
          }
          case "1" =>{
            println("Player1 scores a point!")
            (score1) match {
              case 0 => score1 = 15
              case 15 => score1 = 30
              case 30 => {
                score1 = 40
                if(score2 == 40) println("Deuce")
              }
              case 40 => {
                if(score2 == 40){
                  if(adv1) {
                    score1 = 60
                    endGame = true;
                    println("Player1 wins!")
                  }
                  else {
                    if(adv2) {
                      adv2 = false
                      println("Deuce")
                    }
                    else {
                      adv1 = true
                      println("Advantage Player1")
                    }
                  }
                }
                else {
                  score1 = 60
                  endGame = true;
                }
              }
            }
            println("Player1: " + score1 + " - " + score2 + " :Player2")
          }
          case "2" =>{
            println("Player2 scores a point!")
            (score2) match {
              case 0 => score2 = 15
              case 15 => score2 = 30
              case 30 => {
                score2 = 40
                if(score1 == 40) println("Deuce")
              }
              case 40 => {
                if(score1 == 40){
                  if(adv2) {
                    score2 = 60
                    endGame = true;
                    println("Player2 wins!")
                  }
                  else {
                    if(adv1) {
                      adv1 = false
                      println("Deuce")
                    }
                    else {
                      adv2 = true
                      println("Advantage Player2")
                    }
                  }
                }
                else {
                  score2 = 60
                  endGame = true;
                }
              }

            }
            println("Player1: " + score1 + " - " + score2 + " :Player2")
          }
          case _ => println("Invalid option")
        }
    }
    println("Game finished!")
    println("Restart Game? (Y/N)")
    var ln2 = readLine()
    (ln2) match {
      case "N" => repeat = false
      case "n" => repeat = false
      case "Y" =>
      case "y" =>
      case _ => println("Invalid option")
    }
  }
}